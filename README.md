#Dropbox Explorer#
###Daniel Acedo Calderón###

##Preparación previa##
Para utilizar el SDK de Dropbox con gradle tendremos que incluir la librería en el archivo de configuración de gradle.

![Selección_014.png](https://bitbucket.org/repo/zEBkgX/images/580561703-Selecci%C3%B3n_014.png)

En el archivo manifest tendremos que incluir una actividad propia de la librería de dropbox que utilizará el SDK para la autorización de usuarios. En la entrada scheme tendremos que incluir db-MIAPIKEY. Sustituyendose por la API Key de tu aplicación.

![Selección_020.png](https://bitbucket.org/repo/zEBkgX/images/1542703891-Selecci%C3%B3n_020.png)

Es útil incluir en el archivo strings.xml una entrada con la API Key que nos proporcionará Dropbox de la forma que se explica más adelante. Esta API Key nos hará falta para algunas llamadas a la API.

Para conseguir la API Key tendremos que entrar en la sección de desarrolladores de Dropbox y crear una nueva aplicación https://www.dropbox.com/developers/apps . Despues de crearla rellenando el nombre y el tipo de permisos nos iremos a su página de control.

En la página aparecerá la API Key que tendremos que introducir en los campos indicados anteriormente.

![Selección_018.png](https://bitbucket.org/repo/zEBkgX/images/2590337940-Selecci%C3%B3n_018.png)

##Descripción##
Esta aplicación es una prueba básica para aprender a utilizar el SDK de Java de Dropbox. Pide la autorización del usuario para acceder a su cuenta mediante OAuth2 iniciando sesión en Dropbox y visualiza sus archivos en una lista navegable.

![Selección_010.png](https://bitbucket.org/repo/zEBkgX/images/925319739-Selecci%C3%B3n_010.png)

![Selección_011.png](https://bitbucket.org/repo/zEBkgX/images/3035297875-Selecci%C3%B3n_011.png)

La interfaz consta de dos tabs. En una podemos navegar en los archivos locales de la sdcard del móvil y en otra navegamos sobre los archivos del repositorio de Dropbox.

![Selección_012.png](https://bitbucket.org/repo/zEBkgX/images/3585468952-Selecci%C3%B3n_012.png)

![Selección_013.png](https://bitbucket.org/repo/zEBkgX/images/26081211-Selecci%C3%B3n_013.png)

Si pulsamos sobre una carpeta se mostrarán los archivos que contiene esa carpeta. Podremos volver al nivel anterior pulsando el primer item, que es una carpeta con el título "...".

![Selección_016.png](https://bitbucket.org/repo/zEBkgX/images/1708817762-Selecci%C3%B3n_016.png)

En la pestaña local podemos subir el archivo sobre el que pulsemos al repositorio de Dropbox. Por otro lado si nos situamos en la pestaña de Dropbox al pulsar sobre un archivo podremos descargarlo a la sdcard.

La ruta de destino de la descarga o subida es relativa. Se subirá o bajará al directorio actual sobre el que estemos navegando en la otra pestaña.

Si el archivo que intentamos descargar o bajar es mayor de 1MB se pedirá una confirmación del usuario para continuar con la operación para evitar gastos de datos excesivos sin conocimiento.

![Selección_017.png](https://bitbucket.org/repo/zEBkgX/images/2948329054-Selecci%C3%B3n_017.png)

Por limitaciones ahora mismo no se pueden subir archivos de mas de 150MB de una sentada, habría que hacer multiples peticiones y ahora mismo no está incorporado en la aplicación.